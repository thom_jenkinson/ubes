require 'flickr_fu'

class DynamicController < ApplicationController
  def home
    redirect_to "/new/joinus"
    #@feeds = Feedjira::Feed.fetch_and_parse UBES_BLOG_FEED
  end

  def photos
    flickr = Flickr.new(File.join(Rails.root, "config", "flickr.yml"))
    @photos = flickr.photos.search(group_id: "2388847@N21") 
  end

end
