class BlogController < ApplicationController

  def index

  end
  
  def show
    feeds = Feedjira::Feed.fetch_and_parse UBES_BLOG_FEED

    @blog_post = "<div class='post-outer'><h3>Could not access blog, try again later</h3></div>"

    
    if !feeds.nil?

      if params.has_key?(:post_url)
        post_url = params[:post_url]
      else
        post_url = feeds.entries.first.url
      end
      blog_req = fetch_post(post_url)
      page = Nokogiri::HTML(blog_req.body)
      blog_post_content = page.css(".post-outer").to_s
    end

    if blog_post_content != ""
      @blog_post = blog_post_content
    end

    @months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ]
    
    @archive = Hash.new

    feeds.entries.each do |entry|
      year = entry.published.year.to_s
      month_idx = entry.published.month.to_i() - 1 # Index starts from 0
      if !@archive.has_key?(year)
        @archive[year] = Hash.new
      end
      if !@archive[year].has_key?(month_idx)
        @archive[year][month_idx] = Array.new
      end
      
      @archive[year][month_idx].append(entry)
    end
    
  end

  def fetch
    feeds = Feedjira::Feed.fetch_and_parse UBES_BLOG_FEED
    @blog_post = "<h3>"+ params[:post_url] +"</h3>"
    if !feeds.nil?
      blog_req = fetch_post(params[:post_url])
      page = Nokogiri::HTML(blog_req.body)
      @blog_post = page.css(".post-outer").to_s
      
      
    end
 
    respond_to do |format|
      format.html
      format.js
    end
  end


  private

  def fetch_post(uri, limit=10)
    raise ArgumentError, 'HTTP redirect too deep' if limit == 0

    url = URI.parse(uri)
    req = Net::HTTP::Get.new(url.path)
    response = Net::HTTP.start(url.host, url.port) { |http| http.request(req) }
    case response
    when Net::HTTPSuccess then response
    when Net::HTTPRedirection then fetch_post(response['location'], limit-1)
    else
      response
    end
  end
    
  
end
