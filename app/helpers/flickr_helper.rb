require 'flickr_fu'

module FlickrHelper
  def group_photos(group_id, photo_count = 10)
    flickr = Flickr.new(File.join(Rails.root, 'config', 'flickr.yml'))
    flickr.photos.search(:group_id => group_id).values_at(0..(photo_count-1))
  end

  def render_flickr_images(group_id, photo_count = 10)
    begin
      photos = group_photos(group_id, photo_count)

      render :partial => '/flickr/sidebar', :locals => {:photos => photos}
    #rescue
    #  render :partial => '/flickr/unavailable'
    end
  end

end
